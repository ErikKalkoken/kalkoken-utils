import argparse
import datetime as dt
import re
from pathlib import Path
from typing import List, Tuple

import matplotlib.dates as mdates
import matplotlib.pyplot as plt

from . import __version__

DATETIME_FORMAT = "%m-%d %H-%M"
DATETIME_FORMAT_FILENAME = "%Y%m%dT%H%M"


def main():
    args = parse_arguments()
    input_file = Path(args.filename)
    lines = read_input(input_file)
    blocks_data, blocks_count = parse_into_blocks(lines)
    end_dt = calc_end_dt(args, input_file)
    series, start_dt = create_data_series(
        blocks_data, blocks_count, end_dt, args.max_hours
    )
    output_file = calc_output_filepath(input_file, start_dt, end_dt)
    render_plot(output_file, series)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="memmon-plot",
        description=(
            f"Generate memory consumption charts from memmon logs. (v{__version__})"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("filename")
    parser.add_argument(
        "--end-date",
        help=(
            "Normally the date of the input file is assumed as the end date of the data file. "
            "To see older data this can be overwritten by setting an end date, e.g. '2023-03-13T12:00'"
        ),
    )
    parser.add_argument(
        "--max-hours", type=int, default=24, help="Plot only the last x hours."
    )
    return parser.parse_args()


def calc_end_dt(args, input_file) -> dt.datetime:
    if args.end_date:
        try:
            end_dt = dt.datetime.strptime(args.end_date, "%Y-%m-%dT%H:%M")
        except ValueError:
            print(f"Invalid input for end date: {args.end_date}")
            exit(1)
        print(f"Using provided end date: {end_dt.strftime(DATETIME_FORMAT)}")
        return end_dt
    stat = input_file.stat()
    end_dt = dt.datetime.fromtimestamp(stat.st_mtime)
    print(f"Using end date from input file: {end_dt.strftime(DATETIME_FORMAT)}")
    return end_dt


def read_input(input_file: Path) -> List[str]:
    if not input_file.is_file or not input_file.exists():
        print(f"Not a input_file or does not exist: {input_file.absolute()}")
        exit(1)
    print(f"Reading: {input_file.absolute()}")
    with input_file.open("r") as fp:
        return fp.readlines()


def parse_into_blocks(lines) -> Tuple[List[dict], int]:
    blocks_data = []
    block = 0
    for line in lines:
        if line.startswith("Checking programs"):
            block += 1
        m = re.match(r"RSS of (\S+) is (\d+)", line)
        if m:
            name = m.group(1)
            try:
                value = int(m.group(2))
            except ValueError:
                continue
            blocks_data.append(
                {
                    "name": name,
                    "value": value,
                    "block": block,
                }
            )
    return blocks_data, block


def create_data_series(
    blocks_data: list, blocks_count: int, end_dt: dt.datetime, max_hours: int
) -> Tuple[dict, dt.datetime]:
    max_blocks = 60 * max_hours
    start_dt = end_dt - dt.timedelta(minutes=blocks_count)
    max_blocks = min(blocks_count, max_blocks)
    effective_start_dt = end_dt - dt.timedelta(minutes=max_blocks)
    print(
        "Plotting time frame: "
        f"{effective_start_dt.strftime(DATETIME_FORMAT)} - "
        f"{end_dt.strftime(DATETIME_FORMAT)}"
    )
    series = {}
    for row in blocks_data:
        name = row["name"]
        block = row["block"]
        if block >= (blocks_count - max_blocks):
            if name not in series:
                series[name] = {"x": [], "y": []}
            x = start_dt + dt.timedelta(minutes=block)
            y = row["value"] / 1024 / 1024
            series[name]["x"].append(x)
            series[name]["y"].append(y)
    return series, effective_start_dt


def render_plot(output_file: Path, series: dict) -> None:
    print(f"Writing: {output_file.absolute()}")
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_formatter(mdates.DateFormatter(DATETIME_FORMAT))
    for name, data in series.items():
        ax.plot(data["x"], data["y"], label=name)
    plt.title("Memory usage over time as reported by memmon")
    plt.ylabel("RSS memory usage in MB")
    plt.grid()
    plt.legend()
    fig.savefig(str(output_file))


def calc_output_filepath(
    input_file: Path, start_dt: dt.datetime, end_dt: dt.datetime
) -> Path:
    output_file_name = input_file.with_suffix("")
    start = start_dt.strftime(DATETIME_FORMAT_FILENAME)
    end = end_dt.strftime(DATETIME_FORMAT_FILENAME)
    output_file_name = output_file_name.with_name(
        f"{output_file_name.name}_{start}-{end}"
    )
    output_file_name = output_file_name.with_suffix(".png").name
    output_file = Path.cwd() / output_file_name
    return output_file


if __name__ == "__main__":
    main()
