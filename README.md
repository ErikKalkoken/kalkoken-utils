# Kalkoken Utils

Command line utilities to help admins with Alliance Auth installations.

[![python](https://img.shields.io/badge/python-3.8-blue)](https://gitlab.com/ErikKalkoken/kalkoken-utils)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/kalkoken-utils/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Features

### Memmon Plot

Generate memory consumption charts from memmon log files.

![Memmon Plot Example](https://i.imgur.com/6eQjO0P.png)

## Installation

Please install this package directly from this repo. We recommend to first activate a virtual environment.

```bash
pip install git+https://gitlab.com/ErikKalkoken/kalkoken-utils.git
```

## Usuage

### Memmon Plot

To create a new plot in your current directory you can run this command:

```bash
memmon-plot /home/allianceserver/myauth/log/memmon.log
```

To see what else you can do with Memmon Plot show the help page:

```bash
memmon-plot -h
```

Please note that Memmon Plot generates the plot from memmon's log file. If you do not have a log file yet, you can enable it by adding the below line to your memmon configuration in your `supevisor.conf` file:

```toml
stderr_logfile=/home/allianceserver/myauth/log/memmon.log
```

Please remember to reload your supervisor configuration to enable it.
